import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
	linkActiveClass: 'on',
	routes: [{
		path: '/',
		component: () =>
			import('@/components/Index'),
		children: [{
			path: '',
			name: 'Home',
			component: () =>
				import('@/components/Home'),
			meta: {
				title: '首页'
			}
		}]
	}]
})