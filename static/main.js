// 判断是否是微信浏览器的函数
function isWeiXin() {
	var ua = window.navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i) == 'micromessenger') {
		return true;
	} else {
		return false;
	}
}
// iOS 的版本判断
function get_ios_version() {
	let ua = navigator.userAgent.toLowerCase();
	let version = null;
	if(ua.indexOf("like mac os x") > 0) {
		let reg = /os [\d._]+/gi;
		let v_info = ua.match(reg);
		version = (v_info + "").replace(/[^0-9|_.]/ig, "").replace(/_/ig, "."); // 得到版本号 9.3.2 或者 9.0
		version = parseInt(version.split('.')[0]); // 得到版本号第一位
	}
	return version;
}
// Android 的版本判断
function get_android_version() {
	let ua = navigator.userAgent.toLowerCase();
	let version = null;
	if(ua.indexOf("android") > 0) {
		let reg = /android [\d._]+/gi;
		let v_info = ua.match(reg);
		version = (v_info + "").replace(/[^0-9|_.]/ig, "").replace(/_/ig, "."); // 得到版本号 4.2.2
		version = parseInt(version.split('.')[0]); // 得到版本号第一位
	}
	return version;
}
// 判断是 iOS 还是 Android
function isAndroidOrIos() {
	let u = navigator.userAgent;
	let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1;
	let isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
	if(isAndroid) {
		let type = get_android_version();
		if(type < 5) {
			alert("系统检测到您的 Android 的系统版本过低，有可能无法使用 APP，继续使用请忽略！");
		}
	}
	if(isIOS) {
		let type = get_ios_version();
		if(type < 8) {
			alert("系统检测到您的 IOS 的系统版本过低，有可能无法使用 APP，继续使用请忽略！");
		}
	}
}

window.onload = function() {
	isAndroidOrIos(); // 判断是 iOS 还是 Android
	var tip = document.getElementById('weixin-tip-box');
	if(isWeiXin()) {
		// 是微信浏览器，执行操作
		tip.style.display = 'block';
	} else {
		return false;
	}
}

window.alert = function(name) {
	const iframe = document.createElement('IFRAME');
	iframe.style.display = 'none';
	iframe.setAttribute('src', 'data:text/plain,');
	document.documentElement.appendChild(iframe);
	window.frames[0].window.alert(name);
	iframe.parentNode.removeChild(iframe);
}